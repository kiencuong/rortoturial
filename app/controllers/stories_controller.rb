class StoriesController < ApplicationController
  before_action :set_story, only: [:show, :edit, :update, :destroy, :update_status, :update_status_sprint ]

  # GET /stories
  # GET /stories.json
  def index


    if params[:project_id]
      @stories = Story.where("project_id = ?", params[:project_id])
    else
      @stories = Story.all
    end


  end

  # GET /stories/1
  # GET /stories/1.json
  def show
    @project = Project.find(@story.project_id).name



  end




  # GET /stories/new
  def new
    @story = Story.new

    if params[:id]
        @story.project_id = params[:id]

        #get sprint of project

        @sprints  = Sprint.all

        @sprints  = Sprint.where(project_id:  params[:id])

        #@sprints  = Sprint.find(1).map {|c| [c.name, c.id] }

    end



  end

  # GET /stories/1/edit
  def edit
  end


  #GET  /stories/1/update_status/

  def update_status

    #render plain: @story.name

    @from  = params[:from].to_s
    @to    = params[:to].to_s
    @new_position  = params[:new_position].to_i


    if ( @from  .eql?  "project_backlog" ) &&  (@to .eql? "sprint_backlog")
      @story.backlog = false
      @story.story_status = 1
      puts "move from project to sprint"

    elsif (@from  .eql?  "sprint_backlog") && (@to .eql? "project_backlog")
      @story.backlog = true
      @story.story_status = 0
      puts "move from sprint to project"
    end

   @story.position = @new_position

   #check all other stories in same backlog or sprints and add position increase

    @stories_in_backlog  = Story.where(project_id:  @story.project_id,backlog:true).where('position >= ?',@new_position).order(position: :asc)
    @stories_in_sprint  = Story.where(project_id:  @story.project_id,backlog:false).where('position >= ?',@new_position).order(position: :asc)


    if @story.backlog

      @new_position_story_in_backlog = @new_position
      @stories_in_backlog.each do |story_in_backlog|

        @new_position_story_in_backlog += 1;
        story_in_backlog.position = @new_position_story_in_backlog
        story_in_backlog.save()

      end


    end


    if !@story.backlog

      @new_position_story_in_sprint = @new_position
      @stories_in_sprint.each do |story_in_sprint|
        @new_position_story_in_sprint += 1;
        story_in_sprint.position = @new_position_story_in_sprint
        story_in_sprint.save()

      end
    end




    %#

    @stories_in_backlog_less  = Story.where(project_id:  @story.project_id,backlog:true).where('position < ?',@new_position).order(position: :asc)
    @stories_in_sprint_less  = Story.where(project_id:  @story.project_id,backlog:false).where('position < ?',@new_position).order(position: :asc)

    if @story.backlog
      @new_position_story_in_backlog_less = @new_position

      @stories_in_backlog_less.each do |story_in_backlog|

        @new_position_story_in_backlog_less -= 1;
        story_in_backlog.position = @new_position_story_in_backlog_less
        story_in_backlog.save()

      end

    end

    if !@story.backlog
      @new_position_story_in_sprint_less = @new_position
      @stories_in_sprint_less.each do |story_in_sprint|
        @new_position_story_in_sprint_less -= 1;
        story_in_sprint.position = @new_position_story_in_sprint_less


        story_in_sprint.save()

      end
    end


    #

    @story.save

  end



  #GET  /stories/1/update_status_in_sprint/

  def update_status_sprint

    @from  = params[:from].to_s
    @to    = params[:to].to_s
    @new_position  = params[:new_position].to_i


    if ( @to .eql? "sprint")
      @story.story_status = 0
      puts "move from project to sprint"


    elsif ( @to .eql? "sprint_backlog")
        @story.story_status = 1
        puts "move from project to sprint"

    elsif (@to .eql? "in_progress")
      @story.story_status = 2

    elsif (@to .eql? "sprint_done")
       @story.story_status = 3
    end

    @story.position = @new_position
    @story.save

  end


  # POST /stories
  # POST /stories.json
  def create
    @story = Story.new(story_params)



    respond_to do |format|
      if @story.save
        format.html { redirect_to @story, notice: 'Story was successfully created.' }
        format.json { render :show, status: :created, location: @story }
      else
        format.html { render :new }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /stories/1
  # PATCH/PUT /stories/1.json
  def update
    respond_to do |format|
      if @story.update(story_params)
        format.html { redirect_to @story, notice: 'Story was successfully updated.' }
        format.json { render :show, status: :ok, location: @story }
      else
        format.html { render :edit }
        format.json { render json: @story.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /stories/1
  # DELETE /stories/1.json
  def destroy
    @story.destroy
    respond_to do |format|
      format.html { redirect_to stories_url, notice: 'Story was successfully destroyed.' }
      format.json { head :no_content }
    end
  end




  private
    # Use callbacks to share common setup or constraints between actions.
    def set_story
      @story = Story.find(params[:id])

    end



    # Never trust parameters from the scary internet, only allow the white list through.
    def story_params
      params.require(:story).permit(:name, :description, :backlog, :sprint_id, :project_id, :input, :output, :ratting, :position)
    end
end
