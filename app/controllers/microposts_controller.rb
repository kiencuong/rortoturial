class MicropostsController < ApplicationController

  load_and_authorize_resource

  def show

    @micropost = Micropost.find(params[:id])
    authorize! :manage, @micropost


  end


  def edit

  end
end
