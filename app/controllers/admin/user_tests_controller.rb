class Admin::UserTestsController < ApplicationController
  before_action :set_admin_user_test, only: [:show, :edit, :update, :destroy]

  # GET /admin/user_tests
  # GET /admin/user_tests.json
  def index
    @admin_user_tests = Admin::UserTest.all
  end

  # GET /admin/user_tests/1
  # GET /admin/user_tests/1.json
  def show
  end

  # GET /admin/user_tests/new
  def new
    @admin_user_test = Admin::UserTest.new
  end

  # GET /admin/user_tests/1/edit
  def edit
  end

  # POST /admin/user_tests
  # POST /admin/user_tests.json
  def create
    @admin_user_test = Admin::UserTest.new(admin_user_test_params)

    respond_to do |format|
      if @admin_user_test.save
        format.html { redirect_to @admin_user_test, notice: 'User test was successfully created.' }
        format.json { render :show, status: :created, location: @admin_user_test }
      else
        format.html { render :new }
        format.json { render json: @admin_user_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/user_tests/1
  # PATCH/PUT /admin/user_tests/1.json
  def update
    respond_to do |format|
      if @admin_user_test.update(admin_user_test_params)
        format.html { redirect_to @admin_user_test, notice: 'User test was successfully updated.' }
        format.json { render :show, status: :ok, location: @admin_user_test }
      else
        format.html { render :edit }
        format.json { render json: @admin_user_test.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/user_tests/1
  # DELETE /admin/user_tests/1.json
  def destroy
    @admin_user_test.destroy
    respond_to do |format|
      format.html { redirect_to admin_user_tests_url, notice: 'User test was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_user_test
      @admin_user_test = Admin::UserTest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_user_test_params
      params.require(:admin_user_test).permit(:name, :email)
    end
end
