class SprintsController < ApplicationController
  before_action :set_sprint, only: [:show, :edit, :update, :destroy]

  # GET /sprints
  # GET /sprints.json
  def index
    if params[:project_id]
         @sprints =  Sprint.where("project_id = ?", params[:project_id])

    else
          @sprints = Sprint.all
    end

  end

  # GET /sprints/1
  # GET /sprints/1.json
  def show
    @stories = @sprint.stories.paginate(page: params[:page])

    #get stories in each status
    # 0: project's backlog
    # 1: sprint's  backlog
    # 2: in-progress
    # 3: done


    @stories_in_backlog          = Story.where(project_id:  @sprint.project_id,backlog: true).order(position: :asc)

    @stories_in_sprint_backlog   = Story.where(project_id:  @sprint.project_id,backlog: false,story_status:1).order(position: :asc)

    @stories_in_progress         = Story.where(project_id:  @sprint.project_id,story_status: 2).order(position: :asc)

    @stories_in_done             = Story.where(project_id:  @sprint.project_id,story_status: 3).order(position: :asc)

  end




  # GET /sprints/new
  def new
    @sprint = Sprint.new
  end

  # GET /sprints/1/edit
  def edit
  end

  # POST /sprints
  # POST /sprints.json
  def create
    @sprint = Sprint.new(sprint_params)

    respond_to do |format|
      if @sprint.save
        format.html { redirect_to @sprint, notice: 'Sprint was successfully created.' }
        format.json { render :show, status: :created, location: @sprint }
      else
        format.html { render :new }
        format.json { render json: @sprint.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sprints/1
  # PATCH/PUT /sprints/1.json
  def update
    respond_to do |format|
      if @sprint.update(sprint_params)
        format.html { redirect_to @sprint, notice: 'Sprint was successfully updated.' }
        format.json { render :show, status: :ok, location: @sprint }
      else
        format.html { render :edit }
        format.json { render json: @sprint.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sprints/1
  # DELETE /sprints/1.json
  def destroy
    @sprint.destroy
    respond_to do |format|
      format.html { redirect_to sprints_url, notice: 'Sprint was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sprint
      @sprint = Sprint.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sprint_params
      params.require(:sprint).permit(:name, :project_id)
    end
end
