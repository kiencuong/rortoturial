class StaticPagesController < ApplicationController
  def about
  end

  def help
  end

  def home
  end

  def contact

  end

  def sign_up

  end

  def sign_in

  end
end
