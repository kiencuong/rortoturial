json.array!(@admin_user_tests) do |admin_user_test|
  json.extract! admin_user_test, :id, :name, :email
  json.url admin_user_test_url(admin_user_test, format: :json)
end
