json.array!(@stories) do |story|
  json.extract! story, :id, :name, :description, :backlog, :sprint, :projects_id, :input, :ouput, :ratting
  json.url story_url(story, format: :json)
end
