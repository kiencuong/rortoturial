class Project < ActiveRecord::Base
  belongs_to :user
  has_many :stories, dependent: :destroy
  has_many :sprints, dependent: :destroy
  validates :user_id, presence: true
  validates :name, presence: true, length: { maximum: 30 }

end
