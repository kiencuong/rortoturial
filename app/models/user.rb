class User < ActiveRecord::Base

  has_many :microposts, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :stories, dependent: :destroy

  #before_save { self.email = email.downcase }
  before_create :create_remember_token
  before_save   :downcase_email
  #before_create :create_activation_digest

  validates :name, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
      format:     { with: VALID_EMAIL_REGEX },
      uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, length: { minimum: 6 }, allow_blank: true


  ROLES = %w[admin moderator author banned]


  ROLES = %w[admin moderator author editor]


  def roles=(roles)
    self.roles_mask = (roles & ROLES).map { |r| 2**ROLES.index(r) }.sum
  end

  def roles
    ROLES.reject { |r| ((roles_mask || 0) & 2**ROLES.index(r)).zero? }
  end

  def is?(role)
    roles.include? role.to_s
  end



  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end





  private

    def create_remember_token
     self.remember_token = User.digest(User.new_remember_token)
    end


    def downcase_email
      self.email = email.downcase
    end

    # Creates and assigns the activation token and digest.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end




end
