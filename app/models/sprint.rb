class Sprint < ActiveRecord::Base
  belongs_to :project
  has_many :stories, dependent: :destroy

end
