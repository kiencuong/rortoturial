class Story < ActiveRecord::Base

  belongs_to :project
  belongs_to :sprint
  belongs_to :user
  validates :project_id, presence: true
  validates :sprint_id, presence: true
end
