
/**
 * Created by kiencuong on 4/23/15.
 */


$(document).ready(function() {
    var url  = $('#link').text();
    $( ".column" ).sortable({
        connectWith: ".column",
        //handle: ".portlet-header",
        cancel: ".portlet-toggle",
        start: function (event, ui) {
            ui.item.addClass('tilt');
            tilt_direction(ui.item);

            item = ui.item;
            newList = oldList = ui.item.parent();
        },
        stop: function (event, ui) {
            ui.item.removeClass("tilt");
            $("html").unbind('mousemove', ui.item.data("move_handler"));
            ui.item.removeData("move_handler");

            link  = url.replace("_id_story_", "/"+item.attr('id')+"/");

            $.ajax({
                url: decodeURIComponent("/"+link),
                type: "POST",
                //data: { "article": { "from": oldList.attr('id'), "to": newList.attr('id') } },
                data: { "from": oldList.attr('id'), "to": newList.attr('id'),"new_position":item.index() },
                success: function(resp){
                    //alert(resp);
                }
            });

            //alert("Moved to position: " + item.index() + " from " + oldList.attr('id') + " to " + newList.attr('id'));





        },
        change: function(event, ui) {
            if(ui.sender) newList = ui.placeholder.parent();
            //alert("New poisition :" + ui.index());
        }
    });

    function tilt_direction(item) {
        var left_pos = item.position().left,
            move_handler = function (e) {
                if (e.pageX >= left_pos) {
                    item.addClass("right");
                    item.removeClass("left");
                } else {
                    item.addClass("left");
                    item.removeClass("right");
                }
                left_pos = e.pageX;
            };
        $("html").bind("mousemove", move_handler);
        item.data("move_handler", move_handler);
    }

    $( ".portlet" )
        .addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
        .find( ".portlet-header" )
        .addClass( "ui-widget-header ui-corner-all" )
        .prepend( "<span class='ui-icon ui-icon-minusthick portlet-toggle'></span>");

    $( ".portlet-toggle" ).click(function() {
        var icon = $( this );
        icon.toggleClass( "ui-icon-minusthick ui-icon-plusthick" );
        icon.closest( ".portlet" ).find( ".portlet-content" ).toggle();
    });


});