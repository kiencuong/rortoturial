

$(document).ready(function() {

    var url  = $('#link').text();
    var oldList, newList, item, link;
    $('.sortable').sortable({
        items: "> li:not(:first)",
        start: function(event, ui) {
            item = ui.item;
            newList = oldList = ui.item.parent().parent();
        },
        stop: function(event, ui) {
            //alert("Moved " + item.index() + " from " + oldList.attr('id') + " to " + newList.attr('id'));
            //alert("Attribute: "+ item.attr('id'));

            link  = url.replace("_id_story_", "/"+item.attr('id')+"/");


            $.ajax({
                url: decodeURIComponent("/"+link),
                type: "POST",
                //data: { "article": { "from": oldList.attr('id'), "to": newList.attr('id') } },
                data: { "from": oldList.attr('id'), "to": newList.attr('id'),"new_position":item.index() },
                success: function(resp){
                    //alert(resp);
                }
            });


        },
        change: function(event, ui) {
            if(ui.sender) newList = ui.placeholder.parent().parent();
            //alert("New poisition :" + ui.index());
        },
        connectWith: ".sortable"
    }).disableSelection();

});/**
 * Created by kiencuong on 4/14/15.
 */
