module UsersHelper

  #Returns the full title on a per-page basics
  # @param [Object] page_title
  def full_title(page_title)
    base_title  = "Ruby on Rails Tutorial Sample App"
    if page_title.empty?
      base_title
    else

      "#{page_title} | #{base_title} |  "
    end
  end

  #Use Avatar from Gravatar, just demo, check it out
  # Returns the Gravatar (http://gravatar.com/) for the given user.
  def gravatar_for(user)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
    end
end
