require "spec_helper"

describe Admin::UserTestsController do
  describe "routing" do

    it "routes to #index" do
      get("/admin/user_tests").should route_to("admin/user_tests#index")
    end

    it "routes to #new" do
      get("/admin/user_tests/new").should route_to("admin/user_tests#new")
    end

    it "routes to #show" do
      get("/admin/user_tests/1").should route_to("admin/user_tests#show", :id => "1")
    end

    it "routes to #edit" do
      get("/admin/user_tests/1/edit").should route_to("admin/user_tests#edit", :id => "1")
    end

    it "routes to #create" do
      post("/admin/user_tests").should route_to("admin/user_tests#create")
    end

    it "routes to #update" do
      put("/admin/user_tests/1").should route_to("admin/user_tests#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin/user_tests/1").should route_to("admin/user_tests#destroy", :id => "1")
    end

  end
end
