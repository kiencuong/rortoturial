require 'spec_helper'

describe "admin/user_tests/index" do
  before(:each) do
    assign(:admin_user_tests, [
      stub_model(Admin::UserTest,
        :name => "Name",
        :email => "Email"
      ),
      stub_model(Admin::UserTest,
        :name => "Name",
        :email => "Email"
      )
    ])
  end

  it "renders a list of admin/user_tests" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Email".to_s, :count => 2
  end
end
