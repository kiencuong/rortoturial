require 'spec_helper'

describe "admin/user_tests/edit" do
  before(:each) do
    @admin_user_test = assign(:admin_user_test, stub_model(Admin::UserTest,
      :name => "MyString",
      :email => "MyString"
    ))
  end

  it "renders the edit admin_user_test form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", admin_user_test_path(@admin_user_test), "post" do
      assert_select "input#admin_user_test_name[name=?]", "admin_user_test[name]"
      assert_select "input#admin_user_test_email[name=?]", "admin_user_test[email]"
    end
  end
end
