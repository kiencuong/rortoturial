require 'spec_helper'

describe "admin/user_tests/new" do
  before(:each) do
    assign(:admin_user_test, stub_model(Admin::UserTest,
      :name => "MyString",
      :email => "MyString"
    ).as_new_record)
  end

  it "renders new admin_user_test form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", admin_user_tests_path, "post" do
      assert_select "input#admin_user_test_name[name=?]", "admin_user_test[name]"
      assert_select "input#admin_user_test_email[name=?]", "admin_user_test[email]"
    end
  end
end
