require 'spec_helper'

describe "admin/user_tests/show" do
  before(:each) do
    @admin_user_test = assign(:admin_user_test, stub_model(Admin::UserTest,
      :name => "Name",
      :email => "Email"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Email/)
  end
end
