require 'spec_helper'

describe "stories/show" do
  before(:each) do
    @story = assign(:story, stub_model(Story,
      :name => "Name",
      :description => "Description",
      :backlog => false,
      :sprint => 1,
      :projects => nil,
      :input => "Input",
      :ouput => "Ouput",
      :ratting => 2
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Description/)
    rendered.should match(/false/)
    rendered.should match(/1/)
    rendered.should match(//)
    rendered.should match(/Input/)
    rendered.should match(/Ouput/)
    rendered.should match(/2/)
  end
end
