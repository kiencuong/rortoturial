require 'spec_helper'

describe "stories/index" do
  before(:each) do
    assign(:stories, [
      stub_model(Story,
        :name => "Name",
        :description => "Description",
        :backlog => false,
        :sprint => 1,
        :projects => nil,
        :input => "Input",
        :ouput => "Ouput",
        :ratting => 2
      ),
      stub_model(Story,
        :name => "Name",
        :description => "Description",
        :backlog => false,
        :sprint => 1,
        :projects => nil,
        :input => "Input",
        :ouput => "Ouput",
        :ratting => 2
      )
    ])
  end

  it "renders a list of stories" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => nil.to_s, :count => 2
    assert_select "tr>td", :text => "Input".to_s, :count => 2
    assert_select "tr>td", :text => "Ouput".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
