require 'spec_helper'

describe "stories/new" do
  before(:each) do
    assign(:story, stub_model(Story,
      :name => "MyString",
      :description => "MyString",
      :backlog => false,
      :sprint => 1,
      :projects => nil,
      :input => "MyString",
      :ouput => "MyString",
      :ratting => 1
    ).as_new_record)
  end

  it "renders new story form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", stories_path, "post" do
      assert_select "input#story_name[name=?]", "story[name]"
      assert_select "input#story_description[name=?]", "story[description]"
      assert_select "input#story_backlog[name=?]", "story[backlog]"
      assert_select "input#story_sprint[name=?]", "story[sprint]"
      assert_select "input#story_projects[name=?]", "story[projects]"
      assert_select "input#story_input[name=?]", "story[input]"
      assert_select "input#story_ouput[name=?]", "story[ouput]"
      assert_select "input#story_ratting[name=?]", "story[ratting]"
    end
  end
end
