# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :story do
    name "MyString"
    description "MyString"
    backlog false
    sprint 1
    projects nil
    input "MyString"
    ouput "MyString"
    ratting 1
  end
end
