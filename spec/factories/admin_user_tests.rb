# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :admin_user_test, :class => 'Admin::UserTest' do
    name "MyString"
    email "MyString"
  end
end
