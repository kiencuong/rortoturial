require 'spec_helper'

describe "Static pages" do

  describe "Home page" do

    it "should have the content 'sample'" do
      visit '/static_pages/home'
      #expect(page).to have_content('My Tutorial')
      expect(page).to have_title("Home")
    end
  end

  #describe 'Contact page' do
  #  it "should have the content, 'Contact'" do
  #    visit 'static_pages/contact'
  #    expect(page).to have_content('Contact')
  #  end
  # end
end