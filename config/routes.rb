Rails.application.routes.draw do


  resources :sprints

  resources :stories

  resources :projects

  get 'account_activations/edit'

  get 'users/new'

  #get 'static_pages/about'

  get 'static_pages/home'

  get 'static_pages/help'
  get 'static_pages/sign_up'
  #get 'static_pages/sign_in'
  #get 'static_pages/contact'

  match '/',    to: 'static_pages#home',    via: 'get'
  match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get'
  match '/contact', to: 'static_pages#contact', via: 'get'
  #match '/sign_in', to: 'static_pages#sign_in', via: 'get'
  match '/sign_up', to: 'static_pages#sign_up', via: 'get'
  match '/stories/:id/update_status', to: 'stories#update_status', via: 'post'
  match '/stories/:id/update_status_sprint', to: 'stories#update_status_sprint', via: 'post'

  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :account_activations, only: [:edit]

  #initiate routes for micropost
  resources :microposts


  root  'static_pages#home'
  match '/signup',  to: 'users#new',    via: 'get'
  match '/signin',  to: 'sessions#new', via: 'get'
  match '/signout', to: 'sessions#destroy', via:'delete'


  namespace :projects do
    resources :user_tests
  end



  #scope module: 'admin' do
   # resources :user_tests, :comments
  #end

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
