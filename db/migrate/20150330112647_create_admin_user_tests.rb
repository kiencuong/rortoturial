class CreateAdminUserTests < ActiveRecord::Migration
  def change
    create_table :admin_user_tests do |t|
      t.string :name
      t.string :email

      t.timestamps
    end
  end
end
