class AddUserRefToStories < ActiveRecord::Migration
  def change
    add_reference :stories, :users, index: true
  end
end
