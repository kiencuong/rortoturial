class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :name
      t.string :description
      t.boolean :backlog
      t.references :project, index: true
      t.references :sprint, index: true
      t.string :input
      t.string :output
      t.integer :ratting
      t.integer :story_status, :default => 0

      t.timestamps
    end
  end
end
